<?PHP
// Initialize

class Form
{
	private $entryNum = 0;
	private $fieldArray = Array();

	function __construct($fields)															// Set and build field array
	{
		$this->entryNum = count($fields);
		$this->clean($fields);
		unset($_POST);
	}

	private function clean($fields)
	{
		$this->fieldArray = $fields;
		$keyName = array_keys($fields);
		for($i=0;$this->entryNum>$i;$i++)
		{
			$this->fieldArray[$keyName[$i]] = $this->cleanString($fields[$keyName[$i]]);
		}
	}

	public function cleanString($str, $opt = 'str')
	{
		$OutStr = trim($str);																// Remove WhiteSpace from the beginning and end of str
		$OutStr = stripslashes($str);														// Remove quotes
		switch($opt)
		{
			case "str":
				$OutStr = filter_var($str, FILTER_SANITIZE_STRING);							// Remove any unwanted chars
				break;

			case "num":
				$OutStr = preg_replace('/[^0-9]/i', '', $str);								// Numbers only
				//$str = filter_var($str, FILTER_SANITIZE_NUMBER_INT);
				break;

			case "name":
				$OutStr = preg_replace('/[^a-zA-Z0-9-_]/s', '', $str);						// Remove any unwanted chars
				//$str = filter_var($str, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
				break;

			case "html":
				$OutStr = htmlentities(html_entity_decode($str, ENT_QUOTES, "UTF-8"), ENT_QUOTES, "UTF-8");	// Encode HTML Entities, Strict
				break;

			default:
				$OutStr = preg_replace('/[^a-zA-Z0-9-_]/s', '', $str);
		}
		return $OutStr;
	}

	public function getFields()
	{
		return $this->fieldArray;
	}
}
?>
