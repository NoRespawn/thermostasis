<?PHP
// Initialize

class DB
{
	protected $connect;

	private $dbVars = Array();

	function __construct()
	{
		//Config Vars
		$this->dbVars["user"] = "";
		$this->dbVars["pass"] = "";
		$this->dbVars["server"] = "localhost";
		$this->dbVars["name"] = "";

		//Fuck PDO
		$this->connect = mysqli_connect($this->dbVars["server"], $this->dbVars["user"], $this->dbVars["pass"], $this->dbVars["name"])
		if(!$this->connect)
		{
			//exit("Could not connect: (" . mysqli_connect_error() . ") " . mysqli_connect_error());
		}
	}

	private function q($q)
	{
		return mysqli_query($this->connect, $q);
	}

	protected function sqlGetArray($query)
	{
		$resource = $this->q($query);
		$rows = Array();
		while($row = mysqli_fetch_assoc($resource))
		{
			$rows[] = $row;
		}
		mysqli_free_result($resource);
		return $rows;
	}

	protected function sqlGetNumRows($query)
	{
		$resource = $this->q($query);
		$numRows = mysqli_num_rows($resource);
		mysqli_free_result($resource);
		return $numRows;
	}

	protected function sqlInsertData($query)
	{
		if($this->q($query) === TRUE)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function sqlGetDevice() //-------------------------------------------FIXME
	{
		$d = Array();
		//$d = $this->sqlGetArray("SELECT * FROM device WHERE did = '1'");
		$d["url"] = "http://192.168.1.50";			//remove
		$d["key"] = "AhHhFp4Pa2pqFQy4M7kr";			//remove
		//return $d[0];
		return $d;									//remove
	}

	function __destruct()
	{
        mysqli_close($this->connect);
    }
}
?>
