<?PHP
// Initialize

include("common.php");
include("device.php");

if(isset($_POST))
{
	$Form = new Form($_POST);
	$post = $Form->getFields();

	$keyVals[0] = "mode";									//Keys must match JS values, index must match mcu switch case
	$keyVals[1] = "rmSpt";
	$keyVals[2] = "rmDbd";
	$keyVals[3] = "huSpt";
	$keyVals[4] = "huDbd";
	$keyVals[5] = "oaSpt";
	$keyVals[6] = "oaDbd";
	$keyVals[7] = "dfltSensor";
	$keyVals[8] = "options";								//avgTemps, fanCont
	$keyVals[9] = "rmTmp";
	$keyVals[10] = "oaTmp";
	$keyCt = sizeof($keyVals);

	$error[0] = 0;											//Set to proper index on error
	$error[1] = "Invalid Function Code";
	$error[2] = "Post Value Missing";

	if(isset($post["func"]))
	{
		$Mcu = new Device;

		$response[0] = "No Response";						//Response Status
		$responseCt = 0;

		if($post["func"] == "write")						//Function: Write to arduino
		{
			for($i = 0; $i < $keyCt; $i++)					//Iterate through keys array
			{
				if(isset($post[$keyVals[$i]]))
				{
					if($post[$keyVals[$i]] == "")
					{
						$error[0] = 2;
					}
					else
					{
						$qArr = Array(
							"func" => $post["func"],
							"object" => $i,
							"value" => $post[$keyVals[$i]]
						);

						$response[++$responseCt] = $keyVals[$i];
						$response[++$responseCt] = $Mcu->query($qArr);
						$response[0] = "aok";
					}
				}
			}
		}
		elseif($post["func"] == "read")						//Function: Read from arduino
		{
			$vals = explode(',', $post["val"]);
			$valCt = sizeof($vals);

			for($v = 0; $v < $valCt; $v++)					//Iterate through vals array
			{
				for($i = 0; $i < $keyCt; $i++)				//Iterate through keys array
				{
					if($vals[$v] == $keyVals[$i])
					{
						$qArr = Array(
							"func" => $post["func"],
							"object" => "1",
							"value" => $i
						);

						$response[++$responseCt] = $keyVals[$i];
						$response[++$responseCt] = $Mcu->query($qArr);
						$response[0] = "aok";
					}
				}
			}
		}
		elseif($post["func"] == "ping")						//Function: Heartbeat from arduino
		{
			//$post["status"];
			//$post["temp"];
			//$post["did"];
			toFile($post);	//--------------------------------------------------FIXME
			exit();
		}
		else												//Function: Default
		{
			$error[0] = 1;
		}

		if($error[0] > 0)									//Set response status to error
		{
			$response[0] = $error[$error[0]];
		}

		exit(implode("`", $response));						//Exit with response string
	}
}

function toFile($post)  //------------------------------------------------------FIXME
{
	$file = "debug.txt";
	// Open the file to get existing content
	//$str = file_get_contents($file);
	// Append a new data to the file
	$str = "Post Info: ";

	$str .= var_export($post, true);


	//$headers = apache_request_headers();
	//foreach ($headers as $header => $value)
	//{
    	//$str .= $header. ":" .$value. "\n";
	//}
	// Write the contents back to the file
	file_put_contents($file, $str);
}
?>
<!-- HTML -->
<?PHP include("index.html");?>

<?PHP //eof ?>
