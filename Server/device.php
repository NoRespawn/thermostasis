<?PHP
// Initialize

include("db.php");

class Device extends DB
{
	private $mcuInfo = Array();

	function __construct()
	{
		parent::__construct();

		$this->mcuInfo = $this->sqlGetDevice();
	}

	public function add($post)
	{
		//return $this->addDevice($post);
	}

	public function delete($id)
	{
		//return $this->deleteDevice($id);
	}

	public function clearLog($id)
	{
		//return $this->itsHeavy($id);
	}

	public function viewLog($deviceId, $interval)
	{
		$date = date("mdyHis");
		switch ($interval)
		{
			case "d":
				$interval = date("mdyHis", strtotime("-1 day"));
				break;

			case "w":
				$interval = date("mdyHis", strtotime("-1 week"));
				break;

			case "m":
				$interval = date("mdyHis", strtotime("-1 month"));
				break;

			case "y":
				$interval = date("mdyHis", strtotime("-1 year"));
				break;
		}
		//return $this->itsLog($deviceId, $interval, $date);
	}

	public function query($post)
	{
		$qStr = Array(
			"key" => $this->mcuInfo["key"],
			"func" => $post["func"],
			"object" => $post["object"],
			"value" => $post["value"]
		);
		$httpQuery = http_build_query($qStr);
		return $this->sendPost($httpQuery, $this->mcuInfo["url"]);
	}

	private function sendPost($data, $url)
	{
		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_NOSIGNAL, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
}

/*
READ command
object:
	1 - returns "value" option referenced below
	2 - returns single temp sensor reading. "value" = 1-4 (sensor #) otherwise defTempSensor
value:
	0 - mode
	1 - setpoint
	2 - setpoint delta
	3 - rh setpoint
	4 - rh delta
	5 - oa setpoint
	6 - oa delta
	7 - default temp sensor
	8 - flags
	9 - temp (used by loop, calcd if avgTemp enabled)
	10 - oa temp
flags:
	0x01 - fan continuous
	0x02 - avgTemp ena
	0x04 - NA
*/

/*
WRITE command
object:
	0 - mode
	1 - setpoint
	2 - setpoint delta
	3 - rh setpoint
	4 - rh delta
	5 - oa setpoint
	6 - oa delta
	7 - default temp sensor
	8 - flags
value:
	temps: -20 - 120
	temp delta: 0 - 20
	humid: 0 - 100 (set between (0+[humid delta]/2) and (100-[humid delta]/2))
	humid delta: 0 - 50
	modes: 0-OFF 1-AUTO 2-HEAT 3-COOL 4-FAN
	defTempSensor: 1 - IN_TempSensors[0] (4)
*/
?>
