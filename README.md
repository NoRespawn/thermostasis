# ThermoStasis
HVAC Control with arduino

Init

**Server Setup**
- Install Apache2, PHP7, PHP-Curl, MariaDB
- Enable curl in php.
- Edit db.php with SQL user information.
- Open/Execute setup.php to create tables.

**Arduino Setup** (hvac.h)
- Set devices IP
- Set Host servers IP and Port
- Set HostPath (default '/Server'). This is where index.php resides ex 'www/Server/index.php'.
