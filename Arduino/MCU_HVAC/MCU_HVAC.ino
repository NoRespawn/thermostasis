// INCLUDES

#include "hvac.h"

// STUFF

float getTemp()
{
	if(avgTempEna)
	{
		int tempSensCt = IN_TempSensors[0];
		float val = 0;
		for(int i = 0; i < tempSensCt; i++)
		{
			val += aiTemp(IN_TempSensors[i+1]);
		}
		val /= tempSensCt;
		return val;
	}
	else
	{
		return aiTemp(IN_TempSensors[defTempSensor]);
	}
}

void calcSetpoints()
{
	//Run after setpoint or delta has been changed
	setPointHi = round(setPointTg + ((setPointDt / 100) * setPointTg));
	setPointLo = round(setPointTg - ((setPointDt / 100) * setPointTg));
	setHumidLo = round(setHumidTg - ((setHumidDt / 100) * setHumidTg));
	setOaTmpHi = round(setOaTmpTg + ((setOaTmpDt / 100) * setOaTmpTg));
	setOaTmpLo = round(setOaTmpTg - ((setOaTmpDt / 100) * setOaTmpTg));
}

float aiTemp(int pin)
{
	return analogRead(pin) / tempDiv;	// Analog reading to temperature. Test map(analogRead(pin), 0, 1023, lowTemp, highTemp) function
}

float aiHumid(int pin)
{
	return analogRead(pin) / humidDiv;	// Analog reading to temperature. Test map(analogRead(pin), 0, 1023, lowHumid, highHumid) function
}

void setHvacOut(int state, bool isReady)
{
	//------------------------------------------------------------------ FIXME add continuous operaiton
	if(state != fanState)
	{
		digitalWrite(DO_Fan, HIGH);
		digitalWrite(DO_Heat, HIGH);
		digitalWrite(DO_Cool, HIGH);
		digitalWrite(DO_Humid, HIGH);

		if(isReady)
		{
			fanReady = false;
			
			switch (state)
			{
				case FAN_On:
					digitalWrite(DO_Fan, LOW);
					fanState = state;
					break;
				case FAN_Heat:
					digitalWrite(DO_Heat, LOW);
					fanState = state;
					break;
				case FAN_Cool:
					digitalWrite(DO_Cool, LOW);
					fanState = state;
					break;
				case FAN_Off:
					fanState = state;
					break;
			}
		}
		else
		{
			fanState = FAN_Off;
		}
	}
}

void autoLoop(float temperature, float controlTemp, float controlSetPtLo, float controlSetPtHi)
{
	if(controlTemp < controlSetPtLo || callState == FAN_Heat)
	{
		heatLoop(temperature);
	}
	else if(controlTemp > controlSetPtHi || callState == FAN_Cool)
	{
		coolLoop(temperature);
	}
	else
	{
		callState = FAN_Off;
	}
}
  
void heatLoop(float temperature)
{
	if(temperature < setPointLo)
	{
		callState = FAN_Heat;
	}
	else if(temperature > setPointTg)
	{
		callState = FAN_Off;
	}
}
    
void coolLoop(float temperature)
{
	if(temperature > setPointHi)
	{
		callState = FAN_Cool;
	}
	else if(temperature < setPointTg)
	{
		callState = FAN_Off;
	}
}

float parseQuery(String str)
{
	int keyStart = str.indexOf("key") + 4;			// access key
	int keyEnd = str.indexOf("&", keyStart);
	int fnStart = str.indexOf("func") + 5;			// read or write a value
	int fnEnd = str.indexOf("&", fnStart);
	int objStart = str.indexOf("object") + 7;		// object to read or write
	int objEnd = str.indexOf("&", objStart);
	int valStart = str.indexOf("value") + 6;		// value of object
	int valEnd = str.indexOf("&", valStart);
	
	String key = str.substring(keyStart, keyEnd);	// future: calculate rolling key
	String func = str.substring(fnStart, fnEnd);
	String object = str.substring(objStart, objEnd);
	String value = str.substring(valStart, valEnd);
	int obj = object.toInt();
	int val = value.toInt();

	if(func.compareTo("read") == 0)
	{
		return getValue(obj, val);
	}
	else if(func.compareTo("write") == 0)
	{
		float result = setValue(obj, val);
		calcSetpoints();
		return result;
	}
}

float setValue(int obj, int val)
{
	switch (obj)
	{
		case 0:
			return setMode(val);
		case 1:
			return setVar(setPointTg, -20, 120, val);
		case 2:
			return setVar(setPointDt, 0, 20, val);
		case 3:
			return setVar(setHumidTg, 0, 100, val);
		case 4:
			return setVar(setHumidDt, 0, 50, val);
		case 5:
			return setVar(setOaTmpTg, -20, 120, val);
		case 6:
			return setVar(setOaTmpDt, 0, 20, val);
		case 7:
			return setVar(defTempSensor, 1, IN_TempSensors[0], val);
		case 8:
			return setFlags(val);
		default:
			return ERR;
	}
}

float getValue(int obj, int val)
{
	if(obj == 2)
	{
		switch (val)
		{
			case 1:
				return aiTemp(AI_Temp1);
			case 2:
				return aiTemp(AI_Temp2);
			case 3:
				return aiTemp(AI_Temp3);
			case 4:
				return aiTemp(AI_Temp4);
			default:
				return aiTemp(IN_TempSensors[defTempSensor]);
		}
	}
	switch (val)
	{
		case 0:
			return mode;
		case 1:
			return setPointTg;
		case 2:
			return setPointDt;
		case 3:
			return setHumidTg;
		case 4:
			return setHumidDt;
		case 5:
			return setOaTmpTg;
		case 6:
			return setOaTmpDt;
		case 7:
			return defTempSensor;
		case 8:
			return getFlags();
		case 9:
			return getTemp();
		case 10:
			return aiTemp(AI_OutsideTemp);
		default:
			return ERR;
	}
}

int getFlags()
{
	int flags = 0;
	/*								//unused
	if(mode == FAN)
	{
		flags = flags | 64;
	}
	if(mode == HEAT)
	{
		flags = flags | 32;
	}
	if(mode == COOL)
	{
		flags = flags | 16;
	}
	if(mode == AUTO)
	{
		flags = flags | 8;
	}
	if(digitalRead(DI_Vo))
	{
		flags = flags | 4;
	}
	*/
	if(avgTempEna)
	{
		flags = flags | 2;
	}
	if(fanContinuous)
	{
		flags = flags | 1;
	}
	return flags;
}

int setMode(int reqMode)
{
	if(reqMode == mode)
	{
		mode = OFF;
		return 0;
	}
	else
	{
		switch (reqMode)
		{
			case 0:
				mode = OFF;
				return mode;
			case 1:
				mode = AUTO;
				return mode;
			case 2:
				mode = HEAT;
				return mode;
			case 3:
				mode = COOL;
				return mode;
			case 4:
				mode = FAN;
				return mode;
			default:
				return ERR;
		}
	}
}

float setVar(float &var, float loVal, float hiVal, float val)
{
	if(var < loVal)
	{
		return ERR;
	}
	else if(var > hiVal)
	{
		return ERR;
	}
	else
	{
		var = val;
		return var;
	}
}

int setVar(int &var, int loVal, int hiVal, int val)
{
	if(var < loVal)
	{
		return ERR;
	}
	else if(var > hiVal)
	{
		return ERR;
	}
	else
	{
		var = val;
		return var;
	}
}

int setFlags(int val)
{
	int flags = getFlags();
	int diff = flags ^ val;
	
	if(diff & 2)
	{
		toggleVar(avgTempEna);
	}
	if(diff & 1)
	{
		toggleVar(fanContinuous);
	}
	
	return getFlags();
}

void toggleVar(bool &var)
{
	var = !var;
}

void setup()
{
	int dPins[][3] = {
	{DI_Vo, INPUT, HIGH},
	{DO_Humid, OUTPUT, HIGH},
	{DO_Fan, OUTPUT, HIGH},
	{DO_Heat, OUTPUT, HIGH},
	{DO_Cool, OUTPUT, HIGH},
	{DI_Disable, INPUT, HIGH},
	{DI_Debug, INPUT, HIGH}
	};
	int numPins = sizeof(dPins)/sizeof(dPins[0]);

	for (unsigned long i = 0; i < numPins; i++)
	{
		pinMode(dPins[i][0], dPins[i][1]);
		if(dPins[i][1] == OUTPUT)
		{
			digitalWrite(dPins[i][0], dPins[i][2]);
		}
	}
	
	Serial.begin(115200);
	Serial.println(F("INITIALIZING"));

	//INIT SETPOINTS
	calcSetpoints();
	
	//INIT NETWORK
	byte mac[] = {deviceMac[0], deviceMac[1], deviceMac[2], deviceMac[3], deviceMac[4], deviceMac[5]};
	IPAddress ip(deviceIp[0], deviceIp[1], deviceIp[2], deviceIp[3]);
	IPAddress gateway(deviceGw[0], deviceGw[1], deviceGw[2], deviceGw[3]);
	IPAddress subnet(deviceSn[0], deviceSn[1], deviceSn[2], deviceSn[3]);
	Ethernet.begin(mac, ip, gateway, gateway, subnet);
	server.begin();
}


void loop()
{
	EthernetClient client = server.available();
	unsigned long time = millis();
	static unsigned long time_f = 0;			// fan timer
	static unsigned long time_u = 0;			// humidity timer
	static unsigned long time_h = 0;			// debug timer
	static int changeState = fanState;
	float humidity = aiHumid(AI_Humidity);
	float oaTemperature = aiTemp(AI_OutsideTemp);
	float temperature = getTemp();

	if(digitalRead(DI_Disable))
	{
		setHvacOut(FAN_Off, true);
	}
	else if(digitalRead(DI_Vo))
	{
		setHvacOut(FAN_On, fanReady);
	}
	else
	{
		setHvacOut(callState, fanReady);

		switch (mode)
		{
			case OFF:
				callState = FAN_Off;
				break;
			case FAN:
				callState = FAN_On;
				break;
			case AUTO:
				autoLoop(temperature, oaTemperature, setOaTmpLo, setOaTmpHi);
				break;
			case HEAT:
				heatLoop(temperature);
				break;
			case COOL:
				coolLoop(temperature);
				break;
		}
	}

	if(!fanReady)
	{
		if(reset_f == false)					//start timer
		{
			time_f = time;
			reset_f = true;
		}
		if(time - time_f >= interval_f)		//after interval set to ready
		{
			fanReady = true;
			reset_f = false;
		}
	}
  
	if(digitalRead(DI_EnHumid) && fanState != FAN_Off && humidity < setHumidLo)
	{
		if(reset_u == false)
		{
			time_u = time;
			reset_u = true;
		}
		if(time - time_u >= interval_u && humidStatus == false)
		{
			digitalWrite(DO_Humid, LOW);
			humidStatus = true;
		}
	}
  
	else if(!digitalRead(DI_EnHumid) || humidity > setHumidTg || fanState == FAN_Off)
	{ 
		if(reset_u == true)
		{
			time_u = time;
			reset_u = false;
		}
		if((time - time_u >= 5000 || fanState == FAN_Off || !digitalRead(DI_EnHumid)) && humidStatus == true)
		{
			digitalWrite(DO_Humid, HIGH);
			humidStatus = false;
		}
	}
  
	if(time - time_h >= interval_h || changeState != fanState)              // send heartbeat----------------------------move below client
	{
		Serial.println(F("Sending heartbeat..."));
		int conn = client.connect(hostIP, hostPort);
		if(conn)
		{
			char postContent[50];
			char tmpStr[5];
			dtostrf(temperature, 4, 1, tmpStr);								//dtostrf(var, total len, dec points, buf)
			sprintf (postContent, "func=ping&status=%d&did=%d&temp=%s", fanState, deviceId, tmpStr);
      
			// send heartbeat http
			client.print(F("POST "));
			client.print(hostPath);
			client.println(F("/index.php HTTP/1.0"));
			client.print(F("Cookie: key="));
			client.println(hostKey);
			client.println(F("User-Agent: Arduino"));
			client.println(F("Host: localhost"));
			client.println(F("Content-Type: application/x-www-form-urlencoded"));
			client.print(F("Content-Length:"));
			client.println(strlen(postContent));
			client.println(F("Connection: Keep-Alive"));
			client.println();
			client.print(postContent);
			
			Serial.print(F("Heartbeat Sent: "));
			Serial.println(postContent);
		}
		else
		{
			//Serial.print(F("Error connecting to client... status="));
			//Serial.println(conn);
		}

		/*
		Serial.print(F("FanState = "));
		Serial.print(fanState);
		if(changeState != fanState)
		{
			Serial.print(F(" ----------State Changed!"));
		}
		Serial.println();
		Serial.print(F("Temperature = "));
		Serial.println(temperature);
		//Serial.print(F("Humidity = "));
		//Serial.println(humidity);
		Serial.print(F("Outside Tmp = "));
		Serial.println(oaTemperature);
		Serial.println();
		*/
		
		time_h = time;
		changeState = fanState;
		delay(1);
		client.stop();
	}
	
	//CLIENT CONNECT
	if(client)
	{
		boolean currentLineIsBlank = false;
		boolean postData = false;
		String post = "";
		
		while(client.connected())
		{
			if(client.available())						//data available, receive http request
			{
				char c = client.read();
				if(postData)
				{
					post += c;
				}
				if (c == '\n' && currentLineIsBlank)    // end of header
				{
					postData = true;
				}
				if (c == '\n')                          // starting a new line
				{
					currentLineIsBlank = true;
				} 
				else if (c != '\r')                     // character on the current line
				{
					currentLineIsBlank = false;
				}
			}
			else										//request finished, send http response
			{
				
				float result = parseQuery(post);
				char resultStr[20];
				//itoa(result,resultStr,10); //change result and parseQuery types to long
				dtostrf(result, 4, 1, resultStr); //1 decimal point. Quirk - also on flags...
				int len = strlen(resultStr);
				
				client.println(F("HTTP/1.1 200 OK"));
				client.println(F("Content-Type: text/html"));
				client.print(F("Content-Length: "));
				client.println(len);
				client.println(F("Connection: close"));
				client.println();
				client.println(result);

				Serial.print(F("http request response: "));
				Serial.println(resultStr);
				Serial.println();
          
				break;
			}
		}

		delay(1);
		client.stop();
	}
}
