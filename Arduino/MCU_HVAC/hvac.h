#ifndef __HVAC_H_INCLUDED__
#define __HVAC_H_INCLUDED__

// INCLUDES
#include <SPI.h>
#include <Ethernet.h>

//hvac.h

// Modes
const int OFF = 0;
const int AUTO = 1;
const int HEAT = 2;
const int COOL = 3;
const int FAN = 4;

// Fan States
const int FAN_On = 2001;
const int FAN_Heat = 2002;
const int FAN_Cool = 2003;
const int FAN_Off = 2000;

// DIGITAL PIN DEFS
const int DI_Disable = 12;
const int DI_Debug = 13;
const int DI_Vo = 2;
const int DI_EnHumid = 3;
const int DO_Humid = 6;
const int DO_Fan = 7;
const int DO_Heat = 8;
const int DO_Cool = 9;

// ANALOG PIN DEFS
const int AI_OutsideTemp = 1;
const int AI_Humidity = 2;
const int AI_Temp1 = 0;
const int AI_Temp2 = 3;
const int AI_Temp3 = 4;
const int AI_Temp4 = 5;
const int IN_TempSensors[] = {4, AI_Temp1, AI_Temp2, AI_Temp3, AI_Temp4};	//count, pin, pin, ....

// CONFIG VARS
const unsigned long interval_f = 15000;		// delay fan state changes 15 seconds
const unsigned long interval_u = 10000;		// humidity stable for greater than 1 minute 60000 || 10 sec 10000
const unsigned long interval_h = 10000;		// heartbeat delay
const float tempDiv = 7.5;					// divisor for raw analogread value to temp in deg F
const float humidDiv = 7;					// divisor for raw analogread value to relative humidity
const int ERR = -9999;
const int OK = 1;

// VARIABLES
int mode = AUTO;
int callState = FAN_Off;
int fanState = FAN_Off;
int defTempSensor = 1;		// index for IN_TempSensors[]
float setOaTmpTg = 60.0;
float setOaTmpDt = 3.0;
float setOaTmpLo = 0.0;		//initialized in setup
float setOaTmpHi = 0.0;		//initialized in setup
float setPointTg = 70.0;
float setPointDt = 6.0;
float setPointHi = 0.0;		//initialized in setup
float setPointLo = 0.0;		//initialized in setup
float setHumidTg = 40;
float setHumidDt = 3;
float setHumidLo = 0.0;		//initialized in setup
bool avgTempEna = false;
bool oaTempEna = false;
bool reset_f = false;
bool reset_u = false;
bool humidStatus = false;
bool fanReady = false;
bool fanContinuous = false;

//NETWORK VARS
const unsigned int devicePort = 80;
const byte deviceIp[] = {192, 168, 1, 101};
const byte deviceGw[] = {192, 168, 1, 1};
const byte deviceSn[] = {255, 255, 255, 0};
const byte deviceMac[] = {0xB0, 0x0B, 0x1E, 0x50, 0x00, 0x00};
const unsigned int hostPort = 80;
const byte hostIP[] = {192, 168, 1, 100};
const unsigned int deviceId = 1;
const String deviceKey = "FeatureNotImplemented";		//accessKey
const String hostKey = "FeatureNotImplemented";		    //accessKey
const String hostPath = "/Server";				        //path where index.php resides
EthernetServer server(devicePort);

#endif
